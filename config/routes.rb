Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'calculator#show'
  get 'calculator', to: 'calculator#show', as: :calculator
  post 'calculator', to: 'calculator#calculate'
end
