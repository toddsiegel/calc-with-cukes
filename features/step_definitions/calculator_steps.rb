Given(/^Two operands (\d+) and (\d+)$/) do |operand1, operand2|
  @operand1 = operand1
  @operand2 = operand2
end

When(/^I add them$/) do
  # navigate to URL represented by the calculator_path route
  visit calculator_path

  # Inside the form with the HTML ID 'calculator'
  within('#calculator') do
    # Fill in the 'Operand 1' field with the @operand1 value
    fill_in('Operand 1', with: @operand1)

    # Fill in the 'Operand 2' field with the @operand2 value
    fill_in('Operand 2', with: @operand2)

    # Submit the form
    click_button('Add')
  end
end

Then(/^It equals (\d+)$/) do |result|
  # There should be a div with the ID 'result'
  within('#results') do
    expect(page).to have_text("Result: #{result}")
  end
end
