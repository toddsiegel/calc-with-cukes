class CalculatorController < ApplicationController
  def show; end

  def calculate
    operand1 = params[:operand1].to_i
    operand2 = params[:operand2].to_i

    @result = operand1 + operand2

    render 'show'
  end
end
